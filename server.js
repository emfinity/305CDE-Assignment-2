/* Program objective: Create a RESTful API for CU-305CDE Assignemt part 2.
Clients can access the this RESTful API to search books or do some operations on user accounts */

/* This file (server.js) is used to setup the Node.js server.
It contains all routing which will used by the client. */


/* import modules and create an instance. */
const restify = require('restify');
const getBook = require('./getBook.js');
const handleUser = require('./handleUser.js');

/* create server and listen to port 8080 */
var port = process.env.PORT || 8080;
var server = restify.createServer();
module.exports = server.listen(port, (err) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log('App is ready at : ' + port);
    }
});

/* import the required plugins to parse the body and auth header. */
server.pre(restify.CORS({
    origins: ['*'],
    credentials: true,
    methods: ['GET','PUT','DELETE','POST','OPTIONS'],
    headers: ['Authorization']
}));
restify.CORS.ALLOW_HEADERS.push('authorization');
server.use(restify.fullResponse());
server.use(restify.bodyParser());
server.use(restify.authorizationParser());

// the route for root
// will return a json object which contains a short introduction of the API
server.get('/', (req, res, next) => {
    const return_string = {
        message: "Follow below URL to use this API",
        urls: [
            { searchBooks: "https://emfinity177109948-api.herokuapp.com/search/<book keyword or ISBN>" },
            { userRegister: "https://emfinity177109948-api.herokuapp.com/register" },
            { getUser: "https://emfinity177109948-api.herokuapp.com/user/<username>" },
            { updateUserInfo: "https://emfinity177109948-api.herokuapp.com/user/<username>" },
            { createHistory: "https://emfinity177109948-api.herokuapp.com/search_history" },
            { deleteHistory: "https://emfinity177109948-api.herokuapp.com/search_history/<username>/<timestamp>" }
        ]
    };
    res.json(return_string);
});

// the route for searching book
server.get('/search/:keyword', (req, res, next) => {
    const keyword = req.params.keyword;
    getBook.getBookInfo(keyword).then((result) => {
        res.json(result);
    });
});

// the route for registering new user
server.post('/register', (req, res, next) => {
    const body = req.body;
    const auth = req.authorization;
    const origin = req.headers.origin;
    if((origin === 'https://preview.c9users.io' || origin === 'https://emfinity.gitlab.io') &&
    auth.credentials === '67a80bf19d5a53cb4c87c811ed4a2305') {
        auth.basic.username = 'dbadmin';
        auth.basic.password = 'edu';
    }
    handleUser.register(auth, body).then((data) => {
        res.setHeader('content-type', data.contentType);
        res.send(data.code, data.response);
        res.end();
    }).catch((data) => {
        res.setHeader('content-type', data.contentType);
        res.send(data.code, data.response);
        res.end();
    });
});

// the route for getting user information
server.get('/user/:username', (req, res, next) => {
    const username = req.params.username;
    const auth = req.authorization;
    const origin = req.headers.origin;
    if((origin === 'https://preview.c9users.io' || origin === 'https://emfinity.gitlab.io') &&
    auth.credentials === '67a80bf19d5a53cb4c87c811ed4a2305') {
        auth.basic.username = 'dbadmin';
        auth.basic.password = 'edu';
    }
    handleUser.getUserInfo(auth, username).then((data) => {
        res.setHeader('content-type', data.contentType);
        res.send(data.code, data.response);
        res.end();
    }).catch((data) => {
        res.setHeader('content-type', data.contentType);
        res.send(data.code, data.response);
        res.end();
    });
});

// the route for updating user information
server.put('/user/:username', (req, res, next) => {
    const username = req.params.username;
    const auth = req.authorization;
    const body = req.body;
    const origin = req.headers.origin;
    if((origin === 'https://preview.c9users.io' || origin === 'https://emfinity.gitlab.io') &&
    auth.credentials === '67a80bf19d5a53cb4c87c811ed4a2305') {
        auth.basic.username = 'dbadmin';
        auth.basic.password = 'edu';
    }
    handleUser.updateUserInfo(auth, username, body).then((data) => {
        res.setHeader('content-type', data.contentType);
        res.send(data.code, data.response);
        res.end();
    }).catch((data) => {
        res.setHeader('content-type', data.contentType);
        res.send(data.code, data.response);
        res.end();
    });
});

// the route for storing user's search history
server.post('/search_history', (req, res, next) => {
    const auth = req.authorization;
    const body = req.body;
    const origin = req.headers.origin;
    if((origin === 'https://preview.c9users.io' || origin === 'https://emfinity.gitlab.io') &&
    auth.credentials === '67a80bf19d5a53cb4c87c811ed4a2305') {
        auth.basic.username = 'dbadmin';
        auth.basic.password = 'edu';
    }
    handleUser.createHistory(auth,body).then((data) => {
        res.setHeader('content-type', data.contentType);
        res.send(data.code, data.response);
        res.end();
    }).catch((data) => {
        res.setHeader('content-type', data.contentType);
        res.send(data.code, data.response);
        res.end();
    });
});

// the route for getting user's search histories
server.get('/:username/search_history', (req, res, next) => {
    const username = req.params.username;
    const auth = req.authorization;
    const origin = req.headers.origin;
    if((origin === 'https://preview.c9users.io' || origin === 'https://emfinity.gitlab.io') &&
    auth.credentials === '67a80bf19d5a53cb4c87c811ed4a2305') {
        auth.basic.username = 'dbadmin';
        auth.basic.password = 'edu';
    }
    handleUser.getHistory(auth,username).then((data) => {
        res.setHeader('content-type', data.contentType);
        res.send(data.code, data.response);
        res.end();
    }).catch((data) => {
        res.setHeader('content-type', data.contentType);
        res.send(data.code, data.response);
        res.end();
    });
});

// the route for deleting user's search history
server.del('/search_history/:username/:timestamp', (req, res) => {
    const username = req.params.username;
    const timestamp = req.params.timestamp;
    const auth = req.authorization;
    const origin = req.headers.origin;
    if((origin === 'https://preview.c9users.io' || origin === 'https://emfinity.gitlab.io') &&
    auth.credentials === '67a80bf19d5a53cb4c87c811ed4a2305') {
        auth.basic.username = 'dbadmin';
        auth.basic.password = 'edu';
    }
    handleUser.deleteHistory(auth, username, timestamp).then((data) => {
        res.setHeader('content-type', data.contentType);
        res.send(data.code, data.response);
        res.end();
    }).catch((data) => {
        res.setHeader('content-type', data.contentType);
        res.send(data.code, data.response);
        res.end();
    });
});
