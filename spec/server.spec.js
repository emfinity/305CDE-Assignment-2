/* This file (server.spec.js) is the testing spec for the API */
/* It mainly tests the GET method of the API */
/* The API connection and book searching function will be tested */

var request = require('request');
var server = require('../server.js');
const base_url = 'https://cu-305cde-assignment2-emfinity177109948.c9users.io/';

describe('API Server', () => {
    describe("GET/", () => {
        
        it("should returns status code 200", (done) => {
            request.get(base_url, (err, res, body) => {
                expect(res.statusCode).toBe(200);
                done();
            });
        });

        it('should be able to search books from Google Book API, and get merged books information as result', (done) => {
            // request to search the books by the keyword 'java programming'
            const keyword = encodeURI('java programming');
            request.get(base_url + 'search/' + keyword, (err, res, body) => {
                let json = JSON.parse(body);
                expect(res.statusCode).toBe(200);
                expect(json.length).toBeGreaterThan(0);
                done();
            });
        });
    });
});
