/* This file (database.js) is used to create connection to MongoDB.
It also contains all required functions for CRUD operations on MongoDB */

var mongoose = require('mongoose');
const server = 'mongodb://dbadmin:edu@ds247759.mlab.com:47759/305cde-assignment';
//console.log('connect to server: '+ server);
mongoose.connect(server);
const db = mongoose.connection;

// function for finding a user's record on MongoDB
exports.findUser = (username) => {
    return db.collection('users').find( { 'username': username } );
};

// function for inserting new user on MongoDB
exports.register = (user) => {
    return db.collection('users').insertOne(user);
};

// function for finding user record on MongoDB
exports.getUser = (username) => {
    return db.collection('users').find({ 'username': username.toLowerCase() });
};

// function for updating user information on MongoDB
exports.updateUserInfo = (username,data) => {
    return db.collection('users').updateOne({'username': username}, 
    {$set : {'age': data.age, 'name': data.name, 'password': data.password, 'email': data.email, 'bookmarks': data.bookmarks}});
};

// function for inserting a new user search history on MongoDB
exports.createHistory = (data) => {
    return db.collection('search_histories').insertOne(data);
};

exports.getHistories = (username) => {
    return db.collection('search_histories').find( { 'username': username } );
};

// function for deleting existed user search history on MongoDB
exports.deleteHistory = (username, timestamp) => {
    return db.collection('search_histories').deleteMany( {'username': username, 'timestamp': timestamp} );
};