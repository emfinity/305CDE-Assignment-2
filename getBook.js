/* This file (getBook.js) is used to search and get book information from Google Book API. 
It will return a promise object to server.js after each search operation. */

/* import the HTTP client module */
var request = require('request');

/* parameters which used to setup the complete URL of Google Books API */
const url = 'https://www.googleapis.com/books/v1/volumes';
const subject = 'computers';
const fields = 'items(volumeInfo(title,subtitle,authors,publisher,publishedDate,description,industryIdentifiers,pageCount,categories,imageLinks),saleInfo(listPrice))';

var currencies = {};

// function for searching book using Google Book by inputed keyword
function searchBooksOnGoogle(keyword) {
    return new Promise((resolve, reject) => {
        var books = [];
        // get 10 books
        var query_string = { q: keyword + '+subject:' + subject, printType: 'books', langRestrict: 'en', orderBy: 'newest', fields: fields, maxResults: 25 };
        request.get({ url: url, qs: query_string }, (err, res, body) => {
            if (err) {
                reject(console.log(err));
            }
            else {
                const json = JSON.parse(body);
                for (var book of json.items) {
                    books.push(book);
                }
            }
        });
        setTimeout(() => {
            //if (books.length > 0) {
                resolve(books);
            /*}
            else {
                reject(Error('No result found'));
            }*/
        }, 2000);
    });
}

// function for getting the currency from mycurrency.net(3rd party API)
function getCurrency(keyword) {
    return new Promise((resolve, reject) => {
        searchBooksOnGoogle(keyword).then((books) => {
            //request.get('https://www.mycurrency.net/service/rates', (err, res, body) => {
            request.get('http://openexchangerates.org/api/latest.json?app_id=1ae0f09b7cf74dc1a7b1ee6bc5719425', (err, res, body) => {
                if (err) {
                    reject(console.log(err));
                }
                else {
                    const json = JSON.parse(body);
                    /*for (var currency of json) {
                        if (currency['currency_code'] == 'HKD') {
                            currencies.HKD = currency['rate'];
                        }
                        else if (currency['currency_code'] == 'EUR') {
                            currencies.EUR = currency['rate'];
                        }
                        else if (currency['currency_code'] == 'GBP') {
                            currencies.GBP = currency['rate'];
                        }
                    }*/
                    const exchangeRates = json['rates'];
                    currencies.HKD = exchangeRates['HKD'];
                    currencies.EUR = exchangeRates['EUR'];
                    currencies.GBP = exchangeRates['GBP'];
                    
                    resolve(books);
                }
            });
        }).catch((err) => {
            console.log(err);
        });
    });
}

// function for merging book data and currency data to my own design book's data
function mergeInfo(keyword) {
    var booksGot = [];

    return getCurrency(keyword).then((books) => {
        for (var book of books) {
            if (book['volumeInfo'].industryIdentifiers) {
                new Promise((resolve, reject) => {
                    var bookGot = {};
                    if (book['volumeInfo'].subtitle) {
                        bookGot.title = book['volumeInfo'].title + ' ' + book['volumeInfo'].subtitle;
                    }
                    else {
                        bookGot.title = book['volumeInfo'].title;
                    }
                    bookGot.authors = book['volumeInfo'].authors;
                    bookGot.publisher = book['volumeInfo'].publisher;
                    bookGot.publishedDate = book['volumeInfo'].publishedDate;
                    bookGot.description = book['volumeInfo'].description;
                    bookGot.ISBNs = {};
                    for (var isbn of book['volumeInfo'].industryIdentifiers) {
                        if (isbn.type == 'ISBN_13') {
                            bookGot.ISBNs.ISBN13 = isbn.identifier;
                        }
                        else {
                            bookGot.ISBNs.ISBN10 = isbn.identifier;
                        }
                    }
                    bookGot.pageCount = book['volumeInfo'].pageCount;
                    bookGot.categories = book['volumeInfo'].categories;
                    bookGot.imageLinks = book['volumeInfo']['imageLinks'].thumbnail;
                    bookGot.amounts = {};
                    if (book['saleInfo'] && book['saleInfo']['listPrice'].amount > 0) {
                        // calculate the amount in other currency (HKD, GBP and EUR)
                        bookGot.amounts.USD = (book['saleInfo']['listPrice'].amount).toString();
                        bookGot.amounts.HKD = (book['saleInfo']['listPrice'].amount * currencies.HKD).toFixed(2);
                        bookGot.amounts.GBP = (book['saleInfo']['listPrice'].amount * currencies.GBP).toFixed(2);
                        bookGot.amounts.EUR = (book['saleInfo']['listPrice'].amount * currencies.EUR).toFixed(2);
                    }
                    else {
                        bookGot.amounts.USD = '-';
                        bookGot.amounts.HKD = '-';
                        bookGot.amounts.GBP = '-';
                        bookGot.amounts.EUR = '-';
                    }
                    resolve(bookGot);
                }).then((bookGot) => {
                    booksGot.push(bookGot);
                });
            }
        }
    }).then(() => {
        return new Promise((resolve, reject) => {
            let result = { "results": booksGot };
            resolve(result);
        });
    }).catch((err) => {
        console.log(err);
    });
}

// this module will only export below function
// but this function will run all other functions which in this module
// the running order is: 1. searchBooksOnGoogle 2. getCurrency 3. mergeInfo
exports.getBookInfo = ((keyword) => {
    return mergeInfo(keyword);
});
