/* This file (handleUser.js) is used to handle all operations which related to user.
Such as register user, get/update user information, and delete user's search history. */

/* import database module */
var mongo = require('./database.js');

/* function to check if username has already registered */
function validateUser(username) {
    return new Promise((resolve, reject) => {
        mongo.findUser(username).toArray((err, result) => {
            if (err) {
                reject(Error(err));
            }
            else if (result.length > 0) {
                resolve(false);
            }
            else {
                resolve(true);
            }
        });
    });
}

/* function for registering new user */
exports.register = (auth, body) => {
    return new Promise((resolve, reject) => {
        if (body == null) {
            reject({ code: 400, contentType: 'application/json', response: { status: 'Error', message: 'Invalid body' } });
        }
        else {
            validateUser(body.username).then((valid) => {
                if (auth.basic === undefined) {
                    reject({ code: 401, contentType: 'application/json', response: { status: 'Error', message: 'Missing basic auth' } });
                }
                else if (auth.basic.username !== 'dbadmin' || auth.basic.password !== 'edu') {
                    reject({ code: 401, contentType: 'application/json', response: { status: 'Error', message: 'Invalid credentials' } });
                }
                else if (valid === false) {
                    reject({ code: 400, contentType: 'application/json', response: { status: 'Error', message: 'Username is already existed' } });
                }
                else {
                    mongo.register(body).then(() => {
                        resolve({ code: 201, contentType: 'application/json', response: { status: 'Created', message: 'Register successfully', data: body } });
                    }).catch((err) => {
                        reject({ code: 400, contentType: 'application/json', response: { status: 'Error', message: err } });
                    });
                }
            }).catch((err) => {
                console.log(err);
            });
        }
    });
};

/* function for getting user information */
exports.getUserInfo = (auth, username) => {
    return new Promise((resolve, reject) => {
        if (username == null || username == '') {
            reject({ code: 400, contentType: 'application/json', response: { status: 'Error', message: 'Invalid username' } });
        }
        else if (auth.basic === undefined) {
            reject({ code: 401, contentType: 'application/json', response: { status: 'Error', message: 'Missing basic auth' } });
        }
        else if (auth.basic.username !== 'dbadmin' || auth.basic.password !== 'edu') {
            reject({ code: 401, contentType: 'application/json', response: { status: 'Error', message: 'Invalid credentials' } });
        }
        else {
            mongo.getUser(username).toArray((err, result) => {
                if (err) {
                    reject({ code: 400, contentType: 'application/json', response: { status: 'Error', message: err } });
                }
                if (result.length === 0) {
                    reject({ code: 404, contentType: 'application/json', response: { status: 'Error', message: 'Username not existed' } });
                }
                else {
                    resolve({ code: 200, contentType: 'application/json', response: result[0] });
                }
            });
        }
    });
};

/* function for updating user information */
exports.updateUserInfo = (auth, username, body) => {
    return new Promise((resolve, reject) => {
        if (body == null || username == null || username == '') {
            reject({ code: 400, contentType: 'application/json', response: { status: 'Error', message: 'Invalid body or username' } });
        }
        else {
            validateUser(username).then((valid) => {
                if (auth.basic === undefined) {
                    reject({ code: 401, contentType: 'application/json', response: { status: 'Error', message: 'Missing basic auth' } });
                }
                else if (auth.basic.username !== 'dbadmin' || auth.basic.password !== 'edu') {
                    reject({ code: 401, contentType: 'application/json', response: { status: 'Error', message: 'Invalid credentials' } });
                }
                else if (valid === true) {
                    reject({ code: 404, contentType: 'application/json', response: { status: 'Error', message: 'Username not existed' } });
                }
                else {
                    mongo.updateUserInfo(username, body).then(() => {
                        mongo.getUser(username).toArray((err, result) => {
                            if (err) {
                                reject({ code: 400, contentType: 'application/json', response: { status: 'Error', message: err } });
                            }
                            else {
                                resolve({ code: 200, contentType: 'application/json', response: { status: 'Updated', message: 'Update successfully', data: result[0] } });
                            }
                        });
                    }).catch((err) => {
                        console.log(err);
                    });
                }
            });
        }
    });
};


/* function for creating new search history of user */
exports.createHistory = (auth, body) => {
    return new Promise((resolve, reject) => {
        if (body == null) {
            reject({ code: 400, contentType: 'application/json', response: { status: 'Error', message: 'Invalid body' } });
        }
        else if (auth.basic === undefined) {
            reject({ code: 401, contentType: 'application/json', response: { status: 'Error', message: 'Missing basic auth' } });
        }
        else if (auth.basic.username !== 'dbadmin' || auth.basic.password !== 'edu') {
            reject({ code: 401, contentType: 'application/json', response: { status: 'Error', message: 'Invalid credentials' } });
        }
        else {
            mongo.createHistory(body).then(() => {
                resolve({ code: 201, contentType: 'application/json', response: { status: 'Created', message: 'New history created', data: body } });
            }).catch((err) => {
                reject({ code: 400, contentType: 'application/json', response: { status: 'Error', message: err } });
            });
        }
    });
};

/* function for getting user's search histories */
exports.getHistory = (auth, username) => {
    return new Promise((resolve, reject) => {
        if (username == null || username == '') {
            reject({ code: 400, contentType: 'application/json', response: { status: 'Error', message: 'Invalid username' } });
        }
        else if (auth.basic === undefined) {
            reject({ code: 401, contentType: 'application/json', response: { status: 'Error', message: 'Missing basic auth' } });
        }
        else if (auth.basic.username !== 'dbadmin' || auth.basic.password !== 'edu') {
            reject({ code: 401, contentType: 'application/json', response: { status: 'Error', message: 'Invalid credentials' } });
        }
        else {
            mongo.getHistories(username).toArray((err, result) => {
                if (err) {
                    reject({ code: 400, contentType: 'application/json', response: { status: 'Error', message: err } });
                }
                else {
                    resolve({ code: 200, contentType: 'application/json', response: result });
                }
            });
        }
    });
};

/* function for deleting user search history */
exports.deleteHistory = (auth, username, timestamp) => {
    return new Promise((resolve, reject) => {
        timestamp = parseInt(timestamp);
        if (username == null || username == '') {
            reject({ code: 400, contentType: 'application/json', response: { status: 'Error', message: 'Invalid username' } });
        }
        else if (typeof timestamp == 'undefined' || !timestamp) {
            reject({ code: 400, contentType: 'application/json', response: { status: 'Error', message: 'Invalid timestamp' } });
        }
        else {
            validateUser(username).then((valid) => {
                if (auth.basic === undefined) {
                    reject({ code: 401, contentType: 'application/json', response: { status: 'Error', message: 'Missing basic auth' } });
                }
                else if (auth.basic.username !== 'dbadmin' || auth.basic.password !== 'edu') {
                    reject({ code: 401, contentType: 'application/json', response: { status: 'Error', message: 'Invalid credentials' } });
                }
                else if (valid === true) {
                    reject({ code: 404, contentType: 'application/json', response: { status: 'Error', message: 'Username not existed' } });
                }
                else {
                    mongo.deleteHistory(username, timestamp).then((result) => {
                        resolve({ code: 200, contentType: 'application/json', response: { status: 'Deleted', message: 'Deleted ' + result.deletedCount + ' search history' } });
                    }).catch((err) => {
                        reject({ code: 400, contentType: 'application/json', response: { status: 'Error', message: err } });
                    });
                }
            });
        }
    });
};
